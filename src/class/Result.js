import React, { Component } from 'react';

function Result(props) {
  if(props.value.length!==0){
    return (
      <div className="col-xs-12">
        <div className="col-xs-12">
          <div className="card">
            <div className="card-block text-center">
              <h3>Ciudad : {props.value.name}</h3>
              <h4>Pronóstico : {props.value.weather[0].main} <i className="fab fa-cloudversify"></i></h4>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="card">
            <div className="card-block text-center">
              <h3> Vientos </h3>
              <p className="text-muted">Velocidad: {props.value.wind.speed}</p>
              <p className="text-muted">Grados: {props.value.wind.deg}</p>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="card">
            <div className="card-block text-center">
              <h3>Datos</h3>
              <p className="text-muted"><i className="fas fa-thermometer-three-quarters"></i> Temperatura: {props.value.main.temp}°</p>
              <p className="text-muted"><i className="fas fa-tint"></i> Humedad: {props.value.main.humidity}%</p>
              <p className="text-muted"><i className="fas fa-percent"></i> Presión: {props.value.main.pressure}%</p>
              <p className="text-muted"><i className="fas fa-fire"></i> Temp Máxima: {props.value.main.temp_max}°</p>
              <p className="text-muted"><i className="fas fa-snowflake"></i> Temp Miníma: {props.value.main.temp_min}°</p>
            </div>
          </div>
        </div>
      </div>

    );
  }else{
    return (
      <div className="col-xs-12"></div>
    );
  }

}

export default Result;
