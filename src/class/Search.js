import React, { Component } from 'react';
import Result from './Result';
import Cities from './Cities';
import axios from 'axios';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      cities:[],
    };
    this.handleChange = this.handleChange.bind(this);
  }
  render() {
    return (
      <div className="form-group">
        <div className="col-xs-12">
          <div className="input-group">
            <input onChange={this.handleChange} className="form-control" placeholder="Ciudad" type="text" id="city"></input>
            <button className="btn btn-success" onClick={() => this.getCity()}><i className="fa fa-search"></i></button>
          </div>
        </div>
        {this.drawResult(this.state.data)}
        {this.drawCities(this.state.cities)}
      </div>
    );
  }
  handleChange(event) {
    if(event.target.value.length>=3){
        axios
        .get("http://gd.geobytes.com/AutoCompleteCity?callback=?&q="+event.target.value)
        .then((response) => {
          var data = response.data.substr(2);
          data = data.slice(0, -2);
          data = JSON.parse(data);
          var city_array = [];
          for (var i = 0; i < data.length; i++) {
            city_array[i] = data[i]
          }
          this.setState({cities: city_array});
        })
        .catch((e) =>
        {
          console.error(e);
        });
    }
  }
  drawResult = props=>{
    return <Result value={this.state.data}/>
  }
  drawCities = props=>{
    return <Cities value={this.state.cities}/>
  }
  getCity = props =>{
    let city = document.getElementById('city');
    axios
    .get("https://api.openweathermap.org/data/2.5/weather?q="+city.value+"&APPID=aae57cdf7ca0a5102561100929ce4373")
    .then((response) => {
      //this.setState({name: "RESPONSE TEXT"});
      this.setState({data: response.data});
    })
    .catch((e) =>
    {
      console.error(e);
    });
  }
}

export default Search;
